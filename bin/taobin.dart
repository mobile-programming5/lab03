import 'dart:io';

var listOrder = [];

class Category {
  int cat = 0;
  Recommend r = new Recommend();
  Coffee c = new Coffee();
  Tea t = new Tea();
  MilkCocoa m = new MilkCocoa();
  ProteinShake p = new ProteinShake();
  SodaAndOther s = new SodaAndOther();

  category(int cat) {
    switch (cat) {
      case 1:
        print('Menu');
        r.recommend();
        r.chooseMenu();
        break;
      case 2:
        print('Menu');
        c.coffe();
        c.chooseMenu();
        break;
      case 3:
        print('Menu');
        t.tea();
        t.chooseMenu();
        break;
      case 4:
        print('Menu');
        m.milkCocoa();
        m.chooseMenu();
        break;
      case 5:
        print('Menu');
        p.proteinShake();
        p.chooseMenu();
        break;
      case 6:
        print('Menu');
        s.sodaAndOther();
        s.chooseMenu();
        break;
    }
  }
}

class Recommend {
  var rec;
  void recommend() {
    rec = <int, String>{1: 'LATAE', 2: 'ESPRESSO'};
    rec.forEach((key, value) {
      print(' > $key $value');
    });
  }

  chooseMenu() {
    Recommend r = new Recommend();
    print('Choose menu or Back to Category (enter 0) : ');
    int menu = int.parse(stdin.readLineSync()!);
    if (menu == 0) {
      printCategory();
    } else {
      listOrder.add(rec[menu]);
      print('Choose option : ');
      r.menu(menu);
      choosePay();
    }
  }

  menu(int menu) {
    switch (menu) {
      case 1:
        var latae = <int, String>{
          1: 'Hot 35 Baht',
          2: 'Cool 40 Baht',
          3: 'Smoothie 45 Baht'
        };
        latae.forEach((key, value) {
          print(' > $key $value');
        });
        var lataep = <int, int>{1: 35, 2: 40, 3: 45};
        listOrder.add(lataep[int.parse(stdin.readLineSync()!)]);
        sweet();
        strawAndLid();
        break;
      case 2:
        var esps = <int, String>{
          1: 'Hot 25 Baht',
          2: 'Cool 30 Baht',
        };
        esps.forEach((key, value) {
          print(' > $key $value');
        });
        var espsp = <int, int>{1: 25, 2: 30};
        listOrder.add(espsp[int.parse(stdin.readLineSync()!)]);
        sweet();
        strawAndLid();
        break;
    }
  }
}

class Coffee {
  var cof;
  coffe() {
    cof = <int, String>{1: 'LATAE', 2: 'MOCHA'};
    cof.forEach((key, value) {
      print(' > $key $value');
    });
  }

  chooseMenu() {
    Coffee c = new Coffee();
    print('Choose menu or Back to Category (enter 0) : ');
    int menu = int.parse(stdin.readLineSync()!);
    if (menu == 0) {
      printCategory();
    } else {
      listOrder.add(cof[menu]);
      print('Choose option : ');
      c.menu(menu);
    }
    choosePay();
  }

  menu(int menu) {
    switch (menu) {
      case 1:
      case 2:
        var latae = <int, String>{
          1: 'Hot 35 Baht',
          2: 'Cool 40 Baht',
          3: 'Smoothie 45 Baht'
        };
        latae.forEach((key, value) {
          print(' > $key $value');
        });
        var lataep = <int, int>{1: 35, 2: 40, 3: 45};
        listOrder.add(lataep[int.parse(stdin.readLineSync()!)]);
        sweet();
        strawAndLid();
        break;
    }
  }
}

class Tea {
  var teas;
  tea() {
    teas = <int, String>{1: 'THAI MILK TEA', 2: 'TAIWANESE TEA'};
    teas.forEach((key, value) {
      print(' > $key $value');
    });
  }

  chooseMenu() {
    Tea t = new Tea();
    print('Choose menu or Back to Category (enter 0) : ');
    int menu = int.parse(stdin.readLineSync()!);
    if (menu == 0) {
      printCategory();
    } else {
      listOrder.add(teas[menu]);
      print('Choose option : ');
      t.menu(menu);
    }
    choosePay();
  }

  menu(int menu) {
    switch (menu) {
      case 1:
      case 2:
        var tmt = <int, String>{
          1: 'Hot 35 Baht',
          2: 'Cool 40 Baht',
          3: 'Smoothie 45 Baht'
        };
        tmt.forEach((key, value) {
          print(' > $key $value');
        });
        var tmtp = <int, int>{1: 35, 2: 40, 3: 45};
        listOrder.add(tmtp[int.parse(stdin.readLineSync()!)]);
        sweet();
        strawAndLid();
        break;
    }
  }
}

class MilkCocoa {
  var milk;
  milkCocoa() {
    milk = <int, String>{1: 'MILK', 2: 'COCOA'};
    milk.forEach((key, value) {
      print(' > $key $value');
    });
  }

  chooseMenu() {
    MilkCocoa m = new MilkCocoa();
    print('Choose menu or Back to Category (enter 0) : ');
    int menu = int.parse(stdin.readLineSync()!);
    if (menu == 0) {
      printCategory();
    } else {
      listOrder.add(milk[menu]);
      print('Choose option : ');
      m.menu(menu);
    }
    choosePay();
  }

  menu(int menu) {
    switch (menu) {
      case 1:
      case 2:
        var milk = <int, String>{
          1: 'Hot 30 Baht',
          2: 'Cool 35 Baht',
          3: 'Smoothie 40 Baht'
        };
        milk.forEach((key, value) {
          print(' > $key $value');
        });
        var milkp = <int, int>{1: 30, 2: 35, 3: 40};
        listOrder.add(milkp[int.parse(stdin.readLineSync()!)]);
        sweet();
        strawAndLid();
        break;
    }
  }
}

class ProteinShake {
  var protS;
  void proteinShake() {
    protS = <int, String>{1: 'MILK SHAKE', 2: 'CARAMEL SHAKE'};
    protS.forEach((key, value) {
      print(' > $key $value');
    });
  }

  chooseMenu() {
    ProteinShake p = new ProteinShake();
    print('Choose menu or Back to Category (enter 0) : ');
    int menu = int.parse(stdin.readLineSync()!);
    if (menu == 0) {
      printCategory();
    } else {
      listOrder.add(protS[menu]);
      print('Choose option : ');
      p.menu(menu);
    }
    choosePay();
  }

  menu(int menu) {
    switch (menu) {
      case 1:
      case 2:
        var pm = <int, String>{1: 'Cool 65 Baht'};
        pm.forEach((key, value) {
          print(' > $key $value');
        });
        var pmp = <int, int>{1: 65};
        listOrder.add(pmp[int.parse(stdin.readLineSync()!)]);
        sweet();
        strawAndLid();
        break;
    }
  }
}

class SodaAndOther {
  var sodaO;
  void sodaAndOther() {
    sodaO = <int, String>{1: 'TAO POWER DRINK', 2: 'ICE PLUM SODA'};
    sodaO.forEach((key, value) {
      print(' > $key $value');
    });
  }

  chooseMenu() {
    SodaAndOther s = new SodaAndOther();
    print('Choose menu or Back to Category (enter 0) : ');
    int menu = int.parse(stdin.readLineSync()!);
    if (menu == 0) {
      printCategory();
    } else {
      listOrder.add(sodaO[menu]);
      print('Choose option : ');
      s.menu(menu);
    }
    choosePay();
  }

  menu(int menu) {
    switch (menu) {
      case 1:
        var tspl = <int, String>{1: 'Cool 15 Baht'};
        tspl.forEach((key, value) {
          print(' > $key $value');
        });
        var tsplp = <int, int>{1: 15};
        listOrder.add(tsplp[int.parse(stdin.readLineSync()!)]);
        sweet();
        strawAndLid();
        break;
      case 2:
        var ips = <int, String>{1: 'Cool 25 Baht'};
        ips.forEach((key, value) {
          print(' > $key $value');
        });
        var ipsp = <int, int>{1: 25};
        listOrder.add(ipsp[int.parse(stdin.readLineSync()!)]);
        sweet();
        strawAndLid();
        break;
    }
  }
}

void sweet() {
  print('Select Sweetness Level');
  var sweet = <int, String>{
    1: 'No Sugar',
    2: 'Less Sweet',
    3: 'Just right',
    4: 'Sweet',
    5: 'Very Sweet'
  };
  sweet.forEach((key, value) {
    print(' > $key $value');
  });
  listOrder.add(sweet[int.parse(stdin.readLineSync()!)]);
}

void strawAndLid() {
  var yesNoStraw = <int, String>{
    1: 'Yes,I want a straw',
    2: 'No,I want not a straw'
  };
  print('I want a straw (Yes, No)');
  yesNoStraw.forEach((key, value) {
    print(' > $key $value');
  });
  listOrder.add(yesNoStraw[int.parse(stdin.readLineSync()!)]);

  var yesNoLid = <int, String>{1: 'Yes,I want a lid', 2: 'No,I want not a lid'};
  print('I want a lid (Yes, No)');
  yesNoLid.forEach((key, value) {
    print(' > $key $value');
  });
  listOrder.add(yesNoLid[int.parse(stdin.readLineSync()!)]);
}

void printOrder() {
  print('====== Your Order ======');
  print('Memu : ${listOrder[0]}');
  print('Price : ${listOrder[1]} Baht');
  print('Sweetness Level : ${listOrder[2]}');
  print('Straw : ${listOrder[3]}');
  print('Lid : ${listOrder[4]}');
  print('========================');
}

void choosePay() {
  var pay = <int, String>{
    1: 'Cash',
    2: 'QR Payment',
    3: 'ShopeePay',
    4: 'TAOBIN Credit',
    5: 'Coupon',
  };
  printOrder();
  print('Select Payment Method');
  pay.forEach((key, value) {
    print(' > $key $value');
  });
  int payment = int.parse(stdin.readLineSync()!);
  if (payment == 1) {
    print('Enter amount: ');
    int amount = int.parse(stdin.readLineSync()!);
    int price = listOrder[1];
    int notEnough = price - amount;
    while (notEnough != 0) {
      print('The money received is incomplete. Remaining $notEnough Baht');
      amount = int.parse(stdin.readLineSync()!);
      notEnough = notEnough - amount;
    }
    int change = amount - price;
    if (change == 0) {
      print('NongTao received enough money');
    } else if (amount > price) {
      print('Your change: $change');
    }
  }
  print('(: Successful payment :) \n...Thank you...');
  return;
}

void printCategory() {
  print(
      ' 1. Our Specialty\n 2. Coffee\n 3. Tea\n 4. Milk, Choco & Calamel\n 5. Protein Shake\n 6. Soda & Other');
  int cat = int.parse(stdin.readLineSync()!);
  Category c = new Category();
  c.category(cat);
}

void main() {
  print('>>TAO BIN<<');
  print('Select Category');
  printCategory();
}
