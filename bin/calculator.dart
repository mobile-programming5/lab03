import 'package:calculator/calculator.dart' as calculator;
import 'dart:io';
import 'dart:math';

class Calculator {
  var a = 0;
  var b = 0;
  cal(double a, var operator, double b) {
    switch (operator) {
      case "+":
        return a + b;
      case "-":
        return a - b;
      case "*":
        return a * b;
      case '%':
      case "mod":
        return a % b;
      case "/":
        return a / b;
      case "^":
        return pow(a, b);
    }
  }
}

void main() {
  Calculator c = new Calculator();
  print("Enter num1: ");
  double n1 = double.parse(stdin.readLineSync()!);
  print("Enter operator: ");
  var op = (stdin.readLineSync())!;
  print("Enter num2: ");
  double n2 = double.parse(stdin.readLineSync()!);
  var res = c.cal(n1, op, n2);
  print("result: $res");
}
